---
title: Installing NetHunter On the Samsung Galaxy star2lte s9+
description:
icon:
weight:
author: ["mylesgoose",]
---


# From unpacking to running NetHunter in a number of steps:

1. Flash stock lineageos 20.0 android 13 image following the instructions at lineageos website 
[wiki.lineageos.org/devices/star2lte](https://wiki.lineageos.org/devices/star2lte/)
[download.lineageos.org/devices/star2lte/builds](https://download.lineageos.org/devices/star2lte/builds)
I used lineage-20.0-20231229-nightly-star2lte-signed.zip
[mirrorbits.lineageos.org/full/star2lte/20231229/lineage-20.0-20231229-nightly-star2lte-signed](https://mirrorbits.lineageos.org/full/star2lte/20231229/lineage-20.0-20231229-nightly-star2lte-signed.zip)

SHA256
d167e0feeb356614b8c7b9ed23cbfaf5ce081a3b89783bbfe0d6419d84aadff7 
flash also the boot.img and recovery pertaining to that build.

2. login to the device and make a call and send a message and use the wifi and Bluetooth. Apparently it initializes it. You can install the bloatware from google if you like also as per the instructions on their website.

3. power of the device and hold the up volume button and the bixby button lower left, and the power button. this boots to recovery. select advanced mount/unmount system. then select apply update.

[github.com/topjohnwu/Magisk/releases](https://github.com/topjohnwu/Magisk/releases)
or [magisk.zip](https://gitlab.com/mylesgoose/kali-docs/-/blob/master/nethunter/installing-nethunter-on-the-samsung-star2lte-s9plus/Magisk-v26.4.zip)

[nethunter-20240104_033206-star2lte-los-thirteen-kalifs-full.zip](https://drive.google.com/file/d/1PY6Asf6vXlitrtVC7fAfOeabu-0lOEo4/view?usp=sharing)

or if you wish to build the package yourself here is the files for the nethunter installer (star2lte-los.zip) you can also do it from the source code on the github site referred to in the zip file documents or the lineagos documents above.

[gitlab.com/mylesgoose/kali-docs/-/blob/master/nethunter/installing-nethunter-on-the-samsung-star2lte-s9plus/star2lte-los.zip](https://gitlab.com/mylesgoose/kali-docs/-/blob/master/nethunter/installing-nethunter-on-the-samsung-star2lte-s9plus/star2lte-los.zip?ref_type=heads)

if you decided to build the image yourself you have to put the devices.cfg and kernels.yml files into the correct folders and the modules folder and image remain in the star2lte-los folder from the extracted.zip file shown above, which goes inside the thirteen folder for the NetHunter installer.



This particular image because we have modified the config file it will display an error message when you boot the device. just press okay during boot. It is possible to fix this error message by spoofing the .config file as per this commit. yet i have not done that yet. 
[github.com/yesimxev/android_kernel](https://github.com/yesimxev/android_kernel_xiaomi_laurel_sprout/commit/7a4c36db7373c14bc9715ab1565d32518fcd6531)

[gist.github.com/FreddieOliveira/errormessage](https://gist.github.com/FreddieOliveira/efe850df7ff3951cb62d74bd770dce27)
you need two files on the device the nethunter-20240104_033206-star2lte-los-thirteen-kalifs-full.zip or whatever your one is called and the magisk.apk rename the magisk.apk to magisk.zip,dont use the star2lte-los.zip attached that is for building the kalinethunter.zip with the application available on this site to build yourself. 

you must choose now if you want to preserve encyrpiton and dmverity and disk quote. if you choose to leave it enabled. during install kali will isntall the kernel and then fail to install the apps at the data partition becuse the data partiotin it will be encrypted. this is no big deal as you can then install as below the nethunter.zip file again with magisk moduels install and the device will be booted up and unencrytped then so it will sucseed to install the rest of the apps or you can choose to install the lineageos rom above and then flash this file before rebooting and just prior to isntallign the magisk.zip file [gist.github.com/disabledmverity,diskencyption and quota](https://gitlab.com/mylesgoose/kali-docs/-/blob/master/nethunter/installing-nethunter-on-the-samsung-star2lte-s9plus/Disable_Dm-Verity_ForceEncrypt_quota_11.02.2020.zip) this ensures that when you boot next your data partion is not encypted and once you have booted and setup device and then come back to the recovery installer the below method will fully succeed to install nethunter.zip wihtout then needing to install again with the magisk module method. 
press apply update and select from disk if you have it on your sd card etc or downloaded to internal storage. select the magisk.zip and it will say there is an error with the package its not signed. proceed anyway. reboot the device and then install the magisk.apk login to magisk.apk and you can see it is showing as root. it will ask you do you want to allow it to continue the setup process. select yes. once its rebooted open magisk again. click install it will say, method, option one select and patch a file or option two direct install recommended. select this option direct install. then lets go. follow any onscreen prompts and reboot if necessary. 

4. Run Magisk Manager to finish the rooting process

5. reboot into recovery menu as above. select advanced and mount the system partition again.

4. select apply update and Install the NetHunter zip. this part of the process will fail it will only install the kernel. this is what makes it so that we can access functions that where previously blocked in the lienage os stock kernel. once you have flashed the kali kernel. it takes about 15 min. reboot your device. nothing will look different.  except it will say userdebug or kali as the version of the kernel in the info about phone.and likely display that error message we spoke about earlier.

5. ensure automatic updates for device are turned of by enabling developer options, clicking the about phone/ build number 7 times. this is also a convenient way to use adb to copy files to the sdcard. or you can just download them from the links here actually on the device.

6. open magisk manager. select modules select install from storage and find the build of nethunter downlaoded above form google drive or made yourself.

7. this process takes 25 min or so just be patient.

8. full nethunter will be installed. excluding the boot logo and the background image. you can create them once booted. with the nethunter app.

9. reboot and you will see the apps. 

10. open the nethunter store app and update the terminal and nethunter app. it does not work otherwise. 

11. if you run apt-get dist-update and when prompted if you dont press no and install the repos version of the passwd file it changes the line in passwd file for _apt  and apt-get update will no longer work.  you have to reset it like so 

 1. open kali terminal
 2. type nano /etc/passwd
 4. find the line satrting with  _apt
 5. now edit that line to
 _apt:x:0:65534::/nonexistent:/bin/false
 6. try apt-get update again. it is very importent that you setup Kex manager first before running any updates. or the vnc server will not get setup with a passwd and wil lnot be controlable via the app,

 12. if you try to use the kex manager setup local server for me it failed. it installs it yet does not create a user passwd. so we have to create one manually. open terminal and type vncpasswd
 entre a new passwd twice
 then type vncserver
 this will start the server. 

 13. go back to the kex manager down the bottom of the page it has a refresh button click it and it should say server started. click open kex client and type your passwd click enable audio above prior if you like. 

## Current status

- HID attacks this is enabled in the kernel. to enable it you must open the android terminal and type setprop sys.usbconfig hid
- wifi monitor mode, injection works with external usb device only like rtl88xxau. 
- Docker is enabled in this the kernel also

Also thank your or your help -yesimxev
